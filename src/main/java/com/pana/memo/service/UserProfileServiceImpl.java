package com.pana.memo.service;

import java.io.Console;
import java.util.ArrayList;
import java.util.List;

import com.pana.memo.model.UserProfile;
import com.pana.memo.repository.UserProfileRepository;

import org.springframework.stereotype.Service;


@Service
public class UserProfileServiceImpl implements UserProfileService{
    UserProfileRepository userprofileRepository;

    public UserProfileServiceImpl(UserProfileRepository userprofileRepository) {
        this.userprofileRepository = userprofileRepository ;
    }

    @Override
    public List<UserProfile> getUserProfiles() {
        List<UserProfile> userprofiles = new ArrayList<>();
        userprofileRepository.findAll().forEach(userprofiles::add);
        return userprofiles;
    }

    @Override
    public UserProfile getUserProfileById(Long id) {
        return userprofileRepository.findById(id).get();
    }

    @Override
    public UserProfile getUserProfileByUser(String username) {
        return userprofileRepository.findByUsername(username).orElse(null);

    }

    @Override
    public UserProfile insert(UserProfile userprofile) {
        return userprofileRepository.save(userprofile);
    }

    @Override
    public void updateUserProfile(String username, UserProfile userprofile) {
        UserProfile userprofileFromDb = userprofileRepository.findByUsername(username).get();
        System.out.println(userprofileFromDb.toString());
       
        //userprofileFromDb.setUsername(userprofile.getUsername());
        // userprofileFromDb.setPassword(userprofile.getPassword());
        userprofileFromDb.setEmail(userprofile.getEmail());
        userprofileFromDb.setNickname(userprofile.getNickname());
        userprofileRepository.save(userprofileFromDb);
        
    }

    @Override
    public void deleteUserProfile(Long userprofileId) {
        userprofileRepository.deleteById(userprofileId);
        
    }
    
}
