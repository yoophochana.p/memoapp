package com.pana.memo.service;

import java.util.ArrayList;
import java.util.List;

import com.pana.memo.model.Memo;
import com.pana.memo.repository.MemoRepository;

import org.springframework.stereotype.Service;

@Service
public class MemoServiceImpl implements MemoService {
    MemoRepository memoRepository;

    public MemoServiceImpl(MemoRepository memoRepository) {
        this.memoRepository = memoRepository ;
    }

    @Override
    public List<Memo> getMemos() {
        List<Memo> memos = new ArrayList<>();
        memoRepository.findAll().forEach(memos::add);
        return memos;
    }

    @Override
    public Memo getMemoById(Long id) {
        return memoRepository.findById(id).orElse(null);
    }

    @Override
    public Memo insert(Memo memo) {
        return memoRepository.save(memo);
    }

    @Override
    public void updateMemo(Long id, Memo memo) {
        Memo memoFromDb = memoRepository.findById(id).get();
        System.out.println(memoFromDb.toString());
       
        memoFromDb.setDescription(memo.getDescription());
        memoFromDb.setTitle(memo.getTitle());
        memoRepository.save(memoFromDb);
        
    }

    @Override
    public void deleteMemo(Long memoId) {
        memoRepository.deleteById(memoId);
        
    }
}