package com.pana.memo.service;

import java.util.List;

import com.pana.memo.model.UserProfile;

public interface UserProfileService {
    List<UserProfile> getUserProfiles();

    UserProfile getUserProfileById(Long id);

    UserProfile insert(UserProfile userprofile);

    void updateUserProfile(String username, UserProfile userprofile);

    void deleteUserProfile(Long userprofileId);

    UserProfile getUserProfileByUser(String username);
    
}
