package com.pana.memo.service;

import java.util.List;

import com.pana.memo.model.Memo;

public interface MemoService {
    List<Memo> getMemos();

    Memo getMemoById(Long id);

    Memo insert(Memo memo);

    void updateMemo(Long id, Memo memo);

    void deleteMemo(Long memoId);
}


