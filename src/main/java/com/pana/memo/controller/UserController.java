package com.pana.memo.controller;

import java.net.http.HttpRequest;
import java.util.List;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.pana.memo.model.UserProfile;
import com.pana.memo.service.UserProfileService;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/user")
public class UserController {
    
    UserProfileService userProfileService;

    public UserController (UserProfileService userProfileService) {
        this.userProfileService = userProfileService;
    }
    
    @PostMapping({"/login"})
    public ResponseEntity<String> login(HttpServletRequest request ,@RequestBody UserProfile userProfile ) {

        HttpSession session = request.getSession();
        UserProfile data = userProfileService.getUserProfileByUser(userProfile.getUsername());
        if(data != null && (data.getPassword().equals(userProfile.getPassword())))
        {
            session.setAttribute("username",userProfile.getUsername());
            return new ResponseEntity<String>("login Success",HttpStatus.OK);
        }
        return new ResponseEntity<String>("login Fail",HttpStatus.UNAUTHORIZED);
        // return new ResponseEntity<>(userProfileService.getUserProfileById(userprofileId), HttpStatus.OK);
    }

    @GetMapping({"/logout"})
    public ResponseEntity<String> logout(HttpServletRequest request ) {
        HttpSession session = request.getSession();
        session.removeAttribute("username");
        return new ResponseEntity<String>("logout success",HttpStatus.OK);

        
    }

    @GetMapping
    public ResponseEntity<UserProfile> getUserProfile(HttpServletRequest request  ) {
        HttpSession session = request.getSession();
        UserProfile data = userProfileService.getUserProfileByUser((String)session.getAttribute("username"));
        return new ResponseEntity<>(data, HttpStatus.OK);
    }

    @PostMapping({"/register"})
    public ResponseEntity<UserProfile> saveUserProfile(@RequestBody UserProfile userprofile) {
        UserProfile userprofile1 = userProfileService.insert(userprofile);
        HttpHeaders httpHeaders = new HttpHeaders();
        return new ResponseEntity<>(userprofile1, httpHeaders, HttpStatus.CREATED);
    }

    @PutMapping
    public ResponseEntity<UserProfile> updateUserProfile(HttpServletRequest request, @RequestBody UserProfile userprofile) {
        HttpSession session = request.getSession();

        userProfileService.updateUserProfile((String)session.getAttribute("username"), userprofile);
        return new ResponseEntity<>(userProfileService.getUserProfileByUser((String)session.getAttribute("username")), HttpStatus.OK);
    }



}
