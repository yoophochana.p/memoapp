package com.pana.memo.controller;

import java.util.List;

import com.pana.memo.model.Memo;
import com.pana.memo.service.MemoService;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/memo")
public class MemoController {

    MemoService memoService;

    public MemoController(MemoService memoService) {
        this.memoService = memoService;
    }
    
    @GetMapping
    public ResponseEntity<List<Memo>> getAllMemos() {
        List<Memo> memos = memoService.getMemos();
        return new ResponseEntity<>(memos, HttpStatus.OK);
    }

    @GetMapping({"/{memoId}"})
    public ResponseEntity<Memo> getMemo(@PathVariable Long memoId) {
        Memo memo = memoService.getMemoById(memoId);
        if( memo  == null)return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        return new ResponseEntity<>(memo, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Memo> saveMemo(@RequestBody Memo memo) {
        Memo memo1 = memoService.insert(memo);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("memo", "/api/v1/memo/" + memo1.getId().toString());
        return new ResponseEntity<>(memo1, httpHeaders, HttpStatus.CREATED);
    }

    @PutMapping({"/{memoId}"})
    public ResponseEntity<Memo> updateMemo(@PathVariable("memoId") Long memoId, @RequestBody Memo memo) {
        if(memoService.getMemoById(memoId) == null)return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        memoService.updateMemo(memoId, memo);
        return new ResponseEntity<>(memoService.getMemoById(memoId), HttpStatus.OK);
    }

    @DeleteMapping({"/{memoId}"})
    public ResponseEntity<Memo> deleteMemo(@PathVariable("memoId") Long memoId) {
        if(memoService.getMemoById(memoId) == null)return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        memoService.deleteMemo(memoId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}

