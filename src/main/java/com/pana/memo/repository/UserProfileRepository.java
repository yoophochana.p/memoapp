package com.pana.memo.repository;

import java.util.Optional;

import com.pana.memo.model.UserProfile;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserProfileRepository extends CrudRepository<UserProfile, Long> {

    Optional<UserProfile> findByUsername(String username);

}