package com.pana.memo.model;



import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Memo {
    @Id
    @GeneratedValue
    Long id;

    @Column
    String title;
    
    @Column
    String description;
   
    @CreationTimestamp
    @Column(updatable = false)
    Timestamp dateCreated;

    @Column
    @UpdateTimestamp
    Timestamp lastModified;

}