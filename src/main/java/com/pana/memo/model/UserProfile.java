package com.pana.memo.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.UniqueConstraint;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.context.annotation.Primary;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserProfile {

    @Id
    @GeneratedValue
    Long id;

    @Column(unique = true,nullable = false)
    String username;
    
    @Column(nullable = false)
    @JsonInclude(Include.NON_NULL)
    String password;

    @Column
    String email;

    @Column
    String nickname;
   
    @CreationTimestamp
    @Column(updatable = false)
    Timestamp dateCreated;

    @Column
    @UpdateTimestamp
    Timestamp lastModified;
    
}
