package com.pana.memo.filter;


import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

@Component
@Order(0)
public class AuthFilter extends OncePerRequestFilter{
    

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {

            HttpSession session = request.getSession();
            if(session.getAttribute("username") == null)
            {
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                response.getOutputStream().print("UNAUTHORIZED");
            }
            else
            {
                filterChain.doFilter(request, response);
            }
            
                
        
    }

    @Override
    protected boolean shouldNotFilter(HttpServletRequest request) throws ServletException {
        
        if(request.getRequestURI().contains("login"))return true;
        if(request.getRequestURI().contains("register"))return true;
        return super.shouldNotFilter(request);
    }

    
}
